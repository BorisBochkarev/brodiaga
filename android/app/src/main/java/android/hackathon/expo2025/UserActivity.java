package android.hackathon.expo2025;

import android.hackathon.expo2025.utils.ListPetsAdapter;
import android.nfc.NfcAdapter;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ScrollView;

/* экран для владельца питомца */
public class UserActivity extends AppCompatActivity {

    NfcAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user);

        ((ImageView) findViewById(R.id.back_btn)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                UserActivity.super.onBackPressed();
            }
        });

        // заполнение списка питомцев
        ListView listPets = (ListView) findViewById(R.id.list_pets);
        ListPetsAdapter listAdapter = new ListPetsAdapter(getApplicationContext());
        listPets.setAdapter(listAdapter);
        // независимый от страницы скролл списка
        listPets.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                v.getParent().requestDisallowInterceptTouchEvent(true);
                return false;
            }
        });

        ((ScrollView)findViewById(R.id.main_view)).smoothScrollTo(0,0);
    }
}
