package android.hackathon.expo2025;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.hackathon.expo2025.retrofit.User;
import android.hackathon.expo2025.retrofit.UserInfo;
import android.hackathon.expo2025.utils.Constants;
import android.support.design.widget.FloatingActionButton;
import android.support.multidex.MultiDex;
import android.support.v7.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.json.JSONObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/* основной экран (авторизация, регистрация) */
public class MainActivity extends AppCompatActivity {

    public class TestPost {
        public String login;
        public String password;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // пример отправки POST запроса и получение ответа на него
        TestPost user = new TestPost();
        user.login = "boris";
        user.password = "181910";
        Retrofit retrofit = new Retrofit.Builder().baseUrl(Constants.SERVER_ADDR).
                addConverterFactory(GsonConverterFactory.create()).build();
        User post = retrofit.create(User.class);
        Call<UserInfo> register = post.testPost(user);
        register.enqueue(new Callback<UserInfo>() {
            @Override
            public void onResponse(Call<UserInfo> call, Response<UserInfo> response) {
                Log.d("EXPO2025", response.body().status);
            }
            @Override
            public void onFailure(Call<UserInfo> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "Error " + t.toString(), Toast.LENGTH_SHORT).show();
                Log.d("EXPO2025", "Error: " + t.toString());
            }
        });

        // авторизация
        ((Button) findViewById(R.id.sign_in_button)).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                // логин
                String login = ((EditText) findViewById(R.id.login)).getText().toString();

                switch (login) {
                    case "test": {
                        // если пользователь имеет статус "ловец"
                        SharedPreferences sPref = getSharedPreferences("USER", MODE_PRIVATE);
                        SharedPreferences.Editor editor = sPref.edit();
                        editor.putInt("status_user", 2);
                        editor.apply();
                        Intent intent = new Intent(MainActivity.this, CatchActivity.class);
                        startActivity(intent);
                        break;
                    }
                    case "test1": {
                        // если пользователь имеет статус "владелец"
                        SharedPreferences sPref = getSharedPreferences("USER", MODE_PRIVATE);
                        SharedPreferences.Editor editor = sPref.edit();
                        editor.putInt("status_user", 1);
                        editor.apply();
                        Intent intent = new Intent(MainActivity.this, UserActivity.class);
                        startActivity(intent);
                        break;
                    }
                    case "test2": {
                        // если пользователь имеет статус "доктор"
                        SharedPreferences sPref = getSharedPreferences("USER", MODE_PRIVATE);
                        SharedPreferences.Editor editor = sPref.edit();
                        editor.putInt("status_user", 3);
                        editor.putInt("id_user", 5);
                        editor.apply();
                        Intent intent = new Intent(MainActivity.this, DoctorActivity.class);
                        startActivity(intent);
                        break;
                    }
                }
            }
        });

        // регистрация
        ((Button) findViewById(R.id.sign_up_button)).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                // создание диалогового окна
                final AlertDialog.Builder userDialog = new AlertDialog.Builder(MainActivity.this);
                userDialog.setTitle("Регистрация");

                View dialogSignUp = getLayoutInflater().inflate(R.layout.dialog_user_sign_up, null);
                FloatingActionButton sendRegister = (FloatingActionButton)dialogSignUp.findViewById(R.id.sign_up_send);
                sendRegister.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        // формирование JSON для отправки
                        String JSONStrSignUp = "{'password': '" + ((EditText)dialogSignUp.findViewById(R.id.sign_up_password)).getText() + "', " +
                                "'username': '" + ((EditText) dialogSignUp.findViewById(R.id.sign_up_login)).getText() + "'," +
                                "'FIO':'" + ((EditText) dialogSignUp.findViewById(R.id.sign_up_fio)).getText() + "'," +
                                "'email':'" + ((EditText) dialogSignUp.findViewById(R.id.sign_up_email)).getText() + "'," +
                                "'phone':'" + ((EditText) dialogSignUp.findViewById(R.id.sign_up_phone)).getText() + "'," +
                                "'addr':'" + ((EditText) dialogSignUp.findViewById(R.id.sign_up_addr)).getText() + "'," +
                                "'passport':'" + ((EditText) dialogSignUp.findViewById(R.id.sign_up_passport)).getText() + "'}";
                        Toast.makeText(getApplicationContext(), "SEND", Toast.LENGTH_SHORT).show();
                        Log.d("EXPO2025", JSONStrSignUp);
                        Retrofit retrofit = new Retrofit.Builder().baseUrl(Constants.SERVER_ADDR).
                                addConverterFactory(GsonConverterFactory.create()).build();
                        User auth = retrofit.create(User.class);
                        Call<JSONObject> register = auth.registerUser(JSONStrSignUp);
                        register.enqueue(new Callback<JSONObject>() {
                            @Override
                            public void onResponse(Call<JSONObject> call, Response<JSONObject> response) {
                                Toast.makeText(getApplicationContext(), "OK", Toast.LENGTH_SHORT).show();
                            }
                            @Override
                            public void onFailure(Call<JSONObject> call, Throwable t) {
                                Toast.makeText(getApplicationContext(), "Error " + t.toString(), Toast.LENGTH_SHORT).show();
                                Log.d("EXPO2025", "Error: " + t.toString());
                            }
                        });
                    }
                });

                userDialog.setView(dialogSignUp);
                userDialog.create();
                userDialog.show();
            }
        });
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(newBase);
        MultiDex.install(this);
    }
}

