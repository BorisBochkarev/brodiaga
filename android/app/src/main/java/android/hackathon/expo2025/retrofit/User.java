package android.hackathon.expo2025.retrofit;

import android.hackathon.expo2025.MainActivity;

import org.json.JSONObject;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

/* интерфейс для работы с сервером через Retrofit2 (запросы, связанные с пользователями) */
public interface User {
    @GET("auth/{login}/{password}")
    Call<UserInfo> authUser(@Path("login") String login, @Path("password") String password);

    @GET("register/{login}")
    Call<JSONObject> registerUser(@Path("login") String login);

    @GET("history/add/{id_dock}/{id_pet}")
    Call<JSONObject> addToHistory(@Path("id_dock") int id_dock, @Path("id_pet") int id_pet);

    // для возращаемого JSON объекта необходимо составлять JAVA класс
    @POST("test_post")
    Call<UserInfo> testPost(@Body MainActivity.TestPost obj);
}
