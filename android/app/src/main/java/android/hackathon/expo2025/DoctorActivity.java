package android.hackathon.expo2025;

import android.content.Context;
import android.nfc.NfcAdapter;
import android.support.multidex.MultiDex;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

/* экран для доктора */
public class DoctorActivity extends AppCompatActivity {

    NfcAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_doctor);

        ((ImageView) findViewById(R.id.back_btn)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DoctorActivity.super.onBackPressed();
            }
        });

        adapter = NfcAdapter.getDefaultAdapter(this);
        if (adapter == null) {
            Toast.makeText(getApplicationContext(), "Ошибка доступа к NFC адаптеру", Toast.LENGTH_SHORT).show();
            ((TextView)findViewById(R.id.status_nfc)).setText("Ошибка доступа");
            return;
        } else {
            ((TextView)findViewById(R.id.status_nfc)).setText("Готов к считыванию");
        }
    }
    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(newBase);
        MultiDex.install(this);
    }
}