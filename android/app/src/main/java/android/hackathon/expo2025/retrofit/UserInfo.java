package android.hackathon.expo2025.retrofit;

/* класс пользователя */
public class UserInfo {
    public String status;
    public String login;
    public int status_user;
    public String address;
    public String phone;
    public int passport_series;
    public int passport_number;
    public String href;

    public String to_string(){
        if(status.equals("ok"))
            return login.concat(" ").concat(address).concat(" ").concat(phone);
        else
            return "Status: ".concat(status);
    }

}
