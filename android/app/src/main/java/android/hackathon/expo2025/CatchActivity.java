package android.hackathon.expo2025;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.hackathon.expo2025.retrofit.User;
import android.hackathon.expo2025.utils.Constants;
import android.hackathon.expo2025.utils.ListShelterAdapter;
import android.nfc.NdefMessage;
import android.nfc.NfcAdapter;
import android.os.Parcelable;
import android.support.multidex.MultiDex;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONObject;

import java.io.UnsupportedEncodingException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/* экран для отлова */
public class CatchActivity extends AppCompatActivity {

    NfcAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_catch);

        ((ImageView) findViewById(R.id.back_btn)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CatchActivity.super.onBackPressed();
            }
        });

        // заполнение списка приютов
        ListView listShelter = (ListView) findViewById(R.id.list_shelter);
        ListShelterAdapter listAdapter = new ListShelterAdapter(getApplicationContext());
        listShelter.setAdapter(listAdapter);
        // независимый от страницы скролл
        listShelter.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                v.getParent().requestDisallowInterceptTouchEvent(true);
                return false;
            }
        });


        adapter = NfcAdapter.getDefaultAdapter(this);
        if (adapter == null) {
            Toast.makeText(getApplicationContext(), "Ошибка доступа к NFC адаптеру", Toast.LENGTH_SHORT).show();
            ((TextView)findViewById(R.id.status_nfc)).setText("Ошибка доступа");
            return;
        } else {
            ((TextView)findViewById(R.id.status_nfc)).setText("Готов к считыванию");
        }

        ((ScrollView)findViewById(R.id.main_view)).smoothScrollTo(0,0);
        readFromIntent(getIntent());
    }

    private void readFromIntent(Intent intent) {
        String action = intent.getAction();
        if (NfcAdapter.ACTION_TAG_DISCOVERED.equals(action)
                || NfcAdapter.ACTION_TECH_DISCOVERED.equals(action)
                || NfcAdapter.ACTION_NDEF_DISCOVERED.equals(action)) {
            Parcelable[] rawMsgs = intent.getParcelableArrayExtra(NfcAdapter.EXTRA_NDEF_MESSAGES);
            NdefMessage[] msgs = null;
            if (rawMsgs != null) {
                msgs = new NdefMessage[rawMsgs.length];
                for (int i = 0; i < rawMsgs.length; i++) {
                    msgs[i] = (NdefMessage) rawMsgs[i];
                }
            }
            buildTagViews(msgs);
        }
    }

    private void buildTagViews(NdefMessage[] msgs) {
        if (msgs == null || msgs.length == 0) return;

        String text = "";
        byte[] payload = msgs[0].getRecords()[0].getPayload();
        String textEncoding = ((payload[0] & 128) == 0) ? "UTF-8" : "UTF-16";
        int languageCodeLength = payload[0] & 0063;
        try {
            text = new String(payload, languageCodeLength + 1, payload.length - languageCodeLength - 1, textEncoding);
        } catch (UnsupportedEncodingException e) {
            Toast.makeText(getApplicationContext(), "Ошибка чтения текста", Toast.LENGTH_SHORT).show();
        }

        // метка получена
        ((ImageView) findViewById(R.id.nfc_logo)).setVisibility(View.INVISIBLE);
        ((LinearLayout) findViewById(R.id.pet_info)).setVisibility(View.VISIBLE);
        Log.d("EXPO2025", text);
        ImageView petPhoto = (ImageView) findViewById(R.id.pet_photo);
        petPhoto.setImageResource(R.drawable.tmp_pet);

        SharedPreferences sPref = getSharedPreferences("USER", MODE_PRIVATE);
        int status_user = sPref.getInt("status_user", 0);
        if(status_user == 3){
            int id_dock = sPref.getInt("id_user", 0);
            Retrofit retrofit = new Retrofit.Builder().baseUrl(Constants.SERVER_ADDR).
                    addConverterFactory(GsonConverterFactory.create()).build();
            User auth = retrofit.create(User.class);
            Call<JSONObject> register = auth.addToHistory(id_dock, Integer.valueOf(text));
            register.enqueue(new Callback<JSONObject>() {
                @Override
                public void onResponse(Call<JSONObject> call, Response<JSONObject> response) {
                    Log.d("EXPO2025", response.body().toString());
                }
                @Override
                public void onFailure(Call<JSONObject> call, Throwable t) {
                    Log.d("EXPO2025", "Error: " + t.toString());
                }
            });
        }
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(newBase);
        MultiDex.install(this);
    }
}
