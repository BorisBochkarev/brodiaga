package android.hackathon.expo2025.utils;

import android.content.Context;
import android.content.Intent;
import android.hackathon.expo2025.R;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;

/* адаптер списка, отображающий список приютов */
public class ListShelterAdapter extends BaseAdapter {
    String[] titles = {"ПМСЖ", "ПКС", "Кот Леопольд"};
    String[] address = { "Репина, 24", "Посадская, 3", "Амундсена, 59"};
    String[] phones = {"170-22-44", "047-27-59", "633-70-73"};
    double[] lats = {56.8271958, 56.826148, 56.7979232 };
    double[] lons = {60.5698522, 60.568336, 60.5861924 };
    Context context;
    public ListShelterAdapter(Context context){
        this.context = context;
    }

    @Override
    public int getCount() {
        return address.length;
    }

    @Override
    public Object getItem(int position) {
        return address[position];
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.list_item_shelter, parent, false);

        TextView addr = (TextView) rowView.findViewById(R.id.shelter_addr);
        addr.setText(address[position]);

        TextView phone = (TextView) rowView.findViewById(R.id.shelter_phone);
        phone.setText(phones[position]);

        TextView title = (TextView) rowView.findViewById(R.id.shelter_title);
        title.setText(titles[position]);

        Button toMap = (Button) rowView.findViewById(R.id.to_map);
        toMap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Uri gmmIntentUri = Uri.parse("google.streetview:cbll="+String.valueOf(lats[position])+","+String.valueOf(lons[position]));
                Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                mapIntent.setPackage("com.google.android.apps.maps");
                mapIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(mapIntent);
            }
        });

        return rowView;
    }
}
