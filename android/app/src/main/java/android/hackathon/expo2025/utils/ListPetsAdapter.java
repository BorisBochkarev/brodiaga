package android.hackathon.expo2025.utils;

import android.content.Context;
import android.hackathon.expo2025.R;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

/* адаптер списка, отображающий список питомцев */
public class ListPetsAdapter extends BaseAdapter {
    String[] numbers = { "456", "19823", "910239" };
    String[] names = {"Васька", "Маська", "Баська" };
    String[] breeds = { "Чихуа-хуа", "Хаски", "Офчарка" };
    String[] descriptions = { "Злой", "Добрый", "Просто дурак" };
    Context context;
    public ListPetsAdapter(Context context){
        this.context = context;
    }

    @Override
    public int getCount() {
        return numbers.length;
    }

    @Override
    public Object getItem(int position) {
        return numbers[position];
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.list_item_pets, parent, false);
        TextView number = (TextView) rowView.findViewById(R.id.pets_id);
        number.setText(numbers[position]);
        TextView name = (TextView) rowView.findViewById(R.id.pets_name);
        name.setText(names[position]);
        TextView breed = (TextView) rowView.findViewById(R.id.pets_breed);
        breed.setText(breeds[position]);
        TextView description = (TextView) rowView.findViewById(R.id.pets_description);
        description.setText(descriptions[position]);

//        rowView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent pet = new Intent(context, PetActivity.class);
//                pet.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                context.startActivity(pet);
//            }
//        });

        return rowView;
    }
}
