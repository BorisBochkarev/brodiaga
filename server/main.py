# -*- coding: utf-8 -*-

from flask import Flask, render_template, request, jsonify
import json
import server.dbexpo as db

app = Flask(__name__)
database = db.DB()


# авторизация пользователя
@app.route("/auth/<login>/<password>", methods=['GET'])
def auth(login, password):
    Res = database.SqlQuery(
        "SELECT * FROM user_s WHERE login LIKE '" + login + "' AND password LIKE '" + password + "'")
    if Res == []:
        return jsonify({'status': 'not_found'})
    else:
        print(Res)
        [jRes] = Res
        return jsonify(jRes)


# отправка id всех его животных пользователю
@app.route('/pets/<login>/<id>', methods=['GET'])
def pets(login, id):
    Res = database.SqlQuery(
        "SELECT * FROM user_s WHERE login LIKE '" + login + "' AND id LIKE '" + id + "'")
    if Res == []:
        return jsonify({'status': 'row_not_found'})
    else:
        query = "SELECT id from pets WHERE id_user =  '" + str(id) + "' "
        [ids] = database.SqlQuery(query)
        print(ids)
        return jsonify(ids)


# отправка информации о животном пользователю
@app.route("/pet/<id>/<id_pet>", methods=['GET'])
def pet(id,id_pet):
    query = "SELECT * FROM pets WHERE id = " + str(id_pet) + " AND id_user = " + str(id) + ""
    print(query)
    Res = database.SqlQuery(query)
    if Res == []:
        return jsonify({'status': 'not_found'})
    else:
        print(Res)
        [jRes] = Res
        return jsonify(jRes)

# отправка редактирование животного
@app.route("/pet/set/<id_pet>/<id_mother>/<inoculations>/<sterilization>/<age>/<weight>/<id_breed>/<id_father>/<note>/<id_user>/<status>", methods=['GET'])
def pet_set(id_pet,id_mother,inoculations,sterilization,age,weight,id_breed,id_father,note,id_user,status):
    query = "update pets set id_mother ='" + id_mother + "',inoculations ='" + inoculations + "',sterilization ='" + sterilization + ",age = '" + age + "',weight ='" + weight + "',id_breed = '" + id_breed + "',id_father = '" + id_father + "',note = '" + note + "', id_user = '" + id_user + "',status = '" + status + "' where id = '" + id_pet + "''"
    print(query)
    Res = database.SqlQuery(query)
    if Res == []:
        return jsonify({'status': 'not_found'})
    else:
        print(Res)
        [jRes] = Res
        return jsonify(jRes)


# отправка редактирование животного
@app.route("/pet/add/<id_pet>/<id_mother>/<inoculations>/<sterilization>/<age>/<weight>/<id_breed>/<id_father>/<note>/<id_user>/<status>", methods=['GET'])
def pet_set(id_pet,id_mother,inoculations,sterilization,age,weight,id_breed,id_father,note,id_user,status):
    query = "update pets set id_mother ='" + id_mother + "',inoculations ='" + inoculations + "',sterilization ='" + sterilization + ",age = '" + age + "',weight ='" + weight + "',id_breed = '" + id_breed + "',id_father = '" + id_father + "',note = '" + note + "', id_user = '" + id_user + "',status = '" + status + "' where id = '" + id_pet + "''"
    print(query)
    Res = database.SqlQuery(query)
    if Res == []:
        return jsonify({'status': 'not_found'})
    else:
        print(Res)
        [jRes] = Res
        return jsonify(jRes)

# добавление в историю
@app.route("/history/add/<id_dock>/<id_pet>", methods=['GET'])
def history_add(id_dock, id_pet):
    idNextQuery = "SELECT MAX(id) FROM history"
    [idNext] = database.SqlQuery(idNextQuery)
    if not idNext.get('max'):
        idNextNum = 1
    else:
        idNextNum = idNext.get('max', 1) + 1

    print(idNextNum)
    queryInsert = "INSERT INTO history(id, pid, did, datareg) VALUES ('"+ str(idNextNum) +"', '" + str(id_pet) + "', '" + str(id_dock) + "', NOW()::date)"
    database.SqlQuery(queryInsert)
    return jsonify({'status': 'ok'})

# страница достора
@app.route("/doctor2.html", methods=['GET'])
def doctor2():
    return render_template("doctor2.html")

# отправка истории врачу
@app.route("/history/<id>", methods=['GET'])
def history(id):
    query = "SELECT user_s.fio,user_s.phone,pets.id,pets.id_mother,pets.inoculations,pets.sterilization,pets.age,pets.weight,pets.id_breed,pets.id_father,pets.note,pets.status,history.datareg FROM history,pets,user_s WHERE history.did = " + str(id)+" AND user_s.id = pets.id_user"
    Res = database.SqlQuery(query)
    if Res == []:
        return jsonify({'status': 'not_found'})
    else:
        # print(Res)
        return jsonify(Res)

# регистрация пользователя
@app.route("/register/<reg>", methods=['GET'])
def register(reg):
    JSON = json.loads(reg)
    print(JSON)
    login = JSON['username']
    phone = JSON['phone']
    Rlog = database.SqlQuery("SELECT * FROM user_s  WHERE login LIKE '" + login + "'")
    Rphone = database.SqlQuery("SELECT * FROM user_s  WHERE phone LIKE '" + phone + "'")
    print(Rlog)
    print(Rphone)
    if Rlog != []:
        return jsonify({'status': 'username exist'})
    if Rphone != []:
        return jsonify({'status': 'phone exist'})
    FIO = JSON['FIO']
    print(FIO)
    email = JSON.get('email')
    print(email)
    status = JSON.get('status')
    print(status)
    license_number = JSON.get('license_number')
    print(license_number)
    passport = JSON.get('passport')
    print(passport)
    password = JSON.get('password')
    print(password)
    address = JSON.get('addr')
    print(address)
    [id] = database.SqlQuery("SELECT MAX(id) FROM user_s")
    print(id)
    idd = int(id['max'])
    idd = idd + 1
    print(idd)
    query = "INSERT INTO public.user_s (id,login,phone,address,password,passport_series,href,fio,status,license_number) VALUES ('" + str(
        idd) \
            + "','" + login + "','" + phone + "','" + address + "','" + password + "','" + passport + "','" + email + "','" + FIO + "','" + status + "','" + str(
        license_number) + "')"
    print(query)
    Res = database.SqlQuery(query)
    print(Res)

    print(database.SqlQuery("SELECT * FROM user_s"))
    return jsonify({'status': 'ok'})

# тестирование POST запроса
@app.route("/test_post", methods=['POST'])
def testPost():
    print(request.json)
    return json.dumps({'status':'ok'}), 200


if __name__ == "__main__":
    app.run(debug=True, host='0.0.0.0')
